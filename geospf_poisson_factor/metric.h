/*
 * metric.h
 *
 *  Created on: 31 d�c. 2015
 *      Author: Modou
 */

#include <vector>
#include <iterator>
#include <algorithm>
#include <limits>

#ifndef METRICS_METRIC_H_
#define METRICS_METRIC_H_

namespace metrics {
	class Metric {
		protected:
			std::string name;
			double cumul;
			int nb;
			int nbRecommendations;

			virtual bool contains(uint itemId, std::vector<uint>& test){
				if(std::find(test.begin(), test.end(), itemId) != test.end())
					return true;
				return false;
			}

			virtual double intersect(vector<uint>& recommendations, vector<uint>& realSelection, int nbItems_to_recommend){
				double _intersect = 0.0;
				for(int i=0; i < std::min((int)recommendations.size(), nbItems_to_recommend); i++)
					if(contains(recommendations[i], realSelection))
						_intersect += 1.0;
				return _intersect;
			}

			/**
			 * measures the performance of a prediction and accumulates it.
			 * @param recommendations recommended items
			 * @param realSelection real taken items
			 */
			virtual double score(vector<uint>& recommendations, vector<uint>& realSelection, int nbItems_to_recommend = 30) = 0;

		public:
			/**
			 * Constructor of the class Metric.
			 * It needs in entry the name of the performance measure to evaluate.
			 * @param name a std::string object
			 */
			Metric(std::string name, int nbRecommendations=5) {
				this->name = name;
				this->name.append("_");
				stringstream ss;
				ss << nbRecommendations;
				this->name.append(ss.str());
				this->cumul = 0.0;
				this->nb = 0;
				this->nbRecommendations = nbRecommendations;
			}

			/**
			 * Destructor of a metric evaluator.
			 */
			virtual ~Metric() {}

			/**
			 * returns the name of the performance metric
			 * @return a std::string object
			 */
			virtual string getName(){
				return name;
			}

			/**
			 * resets the accumulations of measure to zero
			 */
			virtual void reset(){
				this->cumul = 0.0;
				this->nb = 0;
			}

			/**
			 * computes the average performance measured over the accumulated values
			 * @return a double
			 */
			virtual double getAverage(){
				if(nb <= 0)
					return 0.0;
				return cumul / nb;
			}

			void measure(vector<pair<int, double>>& listePreds, vector<uint>& realSelection){
				vector<uint> recommendations;
				//----------------------
				for(vector<pair<int, double>>::iterator it=listePreds.begin(); it!=listePreds.end(); it++)
					recommendations.push_back(it->first);
				//----------------------
				this->measure(recommendations, realSelection, nbRecommendations);
			}

			void measure(vector<uint>& recommendations, vector<uint>& realSelection, int nbItems_to_recommend = 30){
				double ascore = score(recommendations, realSelection, nbItems_to_recommend);
				cumul += ascore;
				nb += 1;
			}
	};
}
#endif /* METRICS_METRIC_H_ */
