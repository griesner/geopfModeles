data directory: ../librecSets/

model specification:
	social factorization (SF)   [ social factors only ]

shape and rate hyperparameters:
	tau   (2.000000, 5.000000)

data attributes:
	integer ratings
	undirected network

inference parameters:
	seed:                                     1470400684
	save frequency:                           20
	evaluation frequency:                     -1
	convergence check frequency:              10
	maximum number of iterations:             300
	minimum number of iterations:             30
	change in log likelihood for convergence: 0.000001
	final pass after convergence:             none

using batch variational inference
