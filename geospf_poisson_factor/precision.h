/*
 * precision.h
 *
 *  Created on: 31 d�c. 2015
 *      Author: Modou
 */

#include "metric.h"

#ifndef METRICS_PRECISION_H_
#define METRICS_PRECISION_H_

namespace metrics {
class Precision : public Metric {
protected:
	virtual double score(std::vector<uint>& preds, std::vector<uint>& test, int nbItems_to_recommend = 30){
		if(test.empty() || preds.empty() || nbItems_to_recommend == 0)
			return 0.0;
//		cout << test.size() << ", " << preds.size() << ", " << nbItems_to_recommend << endl;
		return intersect(preds, test, nbItems_to_recommend) / std::min((int)preds.size(), nbItems_to_recommend);
	}

public:
	Precision(std::string name, int nbRecommendations=5) : Metric(name, nbRecommendations){}

	virtual ~Precision(){}
};
}

#endif /* METRICS_PRECISION_H_ */
