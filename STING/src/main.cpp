//============================================================================
// Name        : STING.cpp
// Author      : Dosne
// Description : Hello World in C++, Ansi-style
//============================================================================


using namespace std;
#include "grid.h"

int main(int argc, char *argv[]) {


	Grid *grid = new Grid(atoi(argv[1]),atoi(argv[2]));
	grid->initAllChecks(argv[3]);
	//	grid->exploreAllGrid(grid->getRoot());
	grid->computeClustersAlt(grid->getRoot());
	grid->printClusters();
	grid->gridOut(argv[4]);

	return 0;
}
