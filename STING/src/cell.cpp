/*
 * cell.cpp
 *
 *  Created on: 16 mai 2017
 *      Author: dosne
 */

#include "cell.h"
#include <stdio.h>
#include <string.h>



#define PI 3.14159265

double Cell::DistanceTo(double lat1, double lon1, double lat2, double lon2)
{
	double rlat1 = PI * lat1/180;
	double rlat2 = PI * lat2/180;

	double theta = lon1-lon2;
	double rtheta = PI * theta/180;

	double dist = sin(rlat1) *sin(rlat2) +cos(rlat1) *cos(rlat2) * cos(rtheta);
	dist = acos(dist);
	dist = dist * 180/PI;
	dist = dist * 60 * 1.1515;

	dist = dist * 1.609344;
	return dist;
}

Cell::Cell(bool isRoot, Cell *father, char id, double minLong, double maxLong, double minLat, double maxLat, double centerLong,double centerLat)
{

	this->ru = NULL;
	this->rd = NULL;
	this->lu = NULL;
	this->ld = NULL;
	this->checks = new ListIndexCheckins;
	this->checks->firstElem = NULL;
	this->father = father;
	this->nbUsers = 0;
	this->width = DistanceTo(minLat,minLong,minLat,maxLong);
	this->heigth = DistanceTo(minLat,minLong,maxLat,minLong);
	this->cumulLat = 0;
	this->cumumLong = 0;
	this->gpsCenter = new GpsPoint;
	this->gpsCenter->latitude = centerLat;
	this->gpsCenter->longitude = centerLong;
	this->gpsLeftd = new GpsPoint;
	this->gpsLeftd->latitude = minLat;
	this->gpsLeftd->longitude = minLong;
	this->gpsRightu = new GpsPoint;
	this->gpsRightu->latitude = maxLat;
	this->gpsRightu->longitude = maxLong;
	if(isRoot)
	{
		this->currentLevel = 0;
		this->id = new char[this->currentLevel+2];
		strcpy(this->id,"0");

	}
	else
	{
		this->currentLevel = father->getCurrentLevel()+1;
		this->id = new char[this->currentLevel+2];
		strcpy(this->id,father->id);
		this->id[this->currentLevel] = id;
	}

}

Cell::~Cell(){

	delete this->ru;
	delete this->rd;
	delete this->lu;
	delete this->ld;
	deleteChecksList(this->checks);
	delete this->gpsCenter;
	delete this->gpsLeftd;
	delete this->gpsRightu;
}

void Cell::addChecks(ListIndexCheckins *list, int index,double latitude, double longitude)
{

	IndexChecks *newChecks = new IndexChecks;

	newChecks->index = index;
	newChecks->next = list->firstElem;
	list->firstElem = newChecks;
	this->cumulLat += latitude;
	this->cumumLong += longitude;
}


void Cell::deleteChecksList(ListIndexCheckins *list)
{
	IndexChecks *current = new IndexChecks;
	IndexChecks *next = new IndexChecks;
	current = list->firstElem;
	next = current->next;

	while(next)
	{
		delete current;
		current = next;
		next = current->next;
	}

	delete current;
	delete next;

}

GpsPoint* Cell::getAVGCenterGPS()
{
	GpsPoint *toReturn = new GpsPoint;
	toReturn->latitude = this->cumulLat/(double)this->nbUsers;
	toReturn->longitude = this->cumumLong/(double)this->nbUsers;
	return toReturn;
}


void Cell::init_UL(){
	double minLong, maxLat,centerLong,centerLat;
	minLong = this->gpsLeftd->longitude;
	maxLat = this->gpsRightu->latitude;
	centerLong = this->gpsCenter->longitude;
	centerLat = this->gpsCenter->latitude;


	this->lu = new Cell(false, this,'0',minLong,centerLong,centerLat,maxLat,centerLong+((minLong-centerLong)/(double)2),
			centerLat+((maxLat-centerLat)/(double)2)
	);
}
void Cell::init_UR(){
	double maxLong, maxLat,centerLong,centerLat;
	maxLong = this->gpsRightu->longitude;
	maxLat = this->gpsRightu->latitude;
	centerLong = this->gpsCenter->longitude;
	centerLat = this->gpsCenter->latitude;

	this->ru =  new Cell(false, this,'1',centerLong,maxLong,centerLat, maxLat, centerLong+((maxLong-centerLong)/(double)2),
			centerLat+((maxLat-centerLat)/(double)2));
}
void Cell::init_DL(){
	double minLong, minLat,centerLong,centerLat;
	minLong = this->gpsLeftd->longitude;
	minLat = this->gpsLeftd->latitude;
	centerLong = this->gpsCenter->longitude;
	centerLat = this->gpsCenter->latitude;

	this->ld = new Cell(false, this,'2',minLong,centerLong,minLat,centerLat,centerLong+((minLong-centerLong)/(double)2),
			centerLat+((minLat-centerLat)/(double)2));
}
void Cell::init_DR(){
	double maxLong, minLat,centerLong,centerLat;
	minLat = this->gpsLeftd->latitude;
	centerLong = this->gpsCenter->longitude;
	centerLat = this->gpsCenter->latitude;
	maxLong = this->gpsRightu->longitude;

	this->rd = new Cell(false, this,'3',centerLong,maxLong,minLat,centerLat,centerLong+((maxLong-centerLong)/(double)2),
			centerLat+((minLat-centerLat)/(double)2));

}


double Cell::getArea()
{
	return this->heigth*this->width;
}

double Cell::getDensity()
{
	if(this->getArea() != 0)
		return (double)this->nbUsers/(double)this->getArea();
	else
		return 0;
}

void Cell::print()
{
	if(this->father != NULL)
	{
		//		cout << "==========Father=========="<<endl;
		//		this->father->print();
		//		cout << "===================="<<endl;

	}

	cout << "id : "<< this->id<<endl;
	cout << "min_lat : " << this->gpsLeftd->latitude << " min_lon : "<<this->gpsLeftd->longitude<<endl;
	cout << "max_lat : " << this->gpsRightu->latitude << " max_lon : "<<this->gpsRightu->longitude<<endl;
	cout << "c_lat : " << this->gpsCenter->latitude << " c_lon : "<<this->gpsCenter->longitude<<endl;
	cout << "height : " << this->heigth << " width : "<<this->width << endl;
	cout << "area : " << this->getArea() << endl;
	cout << "density : " << this->getDensity() << endl;
	cout << "current_lvl : "<<this->getCurrentLevel()<< endl;
	cout << "avg_c_lat : " << this->getAVGCenterGPS()->latitude << " avg_c_lon : "<<this->getAVGCenterGPS()->longitude<<endl;


}

Cell* Cell::getLd() {
	return this->ld;
}

Cell* Cell::getLu() {
	return this->lu;
}

Cell* Cell::getRd() {
	return this->rd;
}

Cell* Cell::getRu() {
	return this->ru;
}

void Cell::incrNBUsers() {
	this->nbUsers++;

}

GpsPoint* Cell::getCenterGPS() {

	return this->gpsCenter;
}

int Cell::getNbUsers() {
	return nbUsers;
}

Cell* Cell::getFather() {
	return father;
}

int Cell::getCurrentLevel() {
	return currentLevel;
}

void Cell::printCheck(Checkins *check)
{
	cout << "user : " << check->userID << endl;
	cout << "poi : " << check->poiID << endl;
	cout << "latitude : " << check->coordinates->latitude << endl;
	cout << "longitude : " << check->coordinates->longitude << endl;


}

void Cell::exploreChecksList(ListIndexCheckins *list)
{
	IndexChecks *tmp = list->firstElem;
	while(tmp)
	{
		cout << tmp->index << endl;
		tmp = tmp->next;
	}

	delete tmp;
}

