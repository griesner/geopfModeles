/*
 * cell.h
 */

#include <math.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <thread>
#include <fstream>
#include <omp.h>
#include <math.h>
#include <stdlib.h>

using namespace std;

//GPS Point
typedef struct gpsPoint GpsPoint;
struct gpsPoint{

	double latitude;
	double longitude;

};


//Object to record in each clusters
typedef struct checkins Checkins;
struct checkins{

	int userID;
	int poiID;
	GpsPoint *coordinates;
	double time;
	Checkins *next;
};

typedef struct indexChecks IndexChecks;
struct indexChecks{
	int index;
	IndexChecks *next;
};

typedef struct listIndexCheckins ListIndexCheckins;
struct listIndexCheckins{

	IndexChecks *firstElem;
};

class Cell{

private:
	Cell *father;
	Cell *lu; // son left upper cell
	Cell *ru; //son right upper cell
	Cell *ld; //son left down cell
	Cell *rd; //son right down cell
	int nbUsers; //NB users inside cell
	GpsPoint *gpsLeftd; //left down gps point from cell
	GpsPoint *gpsRightu; //right upper gps point from cell
	GpsPoint *gpsCenter; //center point from cell
	double cumulLat;
	double cumumLong;
	double width; //Cell s Width
	double heigth; //Cell s Heigth
	int currentLevel;//Curent level in the tree

public:
	char *id; //Cell s ID
	ListIndexCheckins *checks;
	Cell(bool isRoot, Cell *father, char id, double minLong, double maxLong, double minLat, double maxLat, double centerLong,double centerLat);
	~Cell();
	void addChecks(ListIndexCheckins *list, int index, double latitude, double longitude);
	void deleteChecksList(ListIndexCheckins *list);
	GpsPoint *getAVGCenterGPS();
	GpsPoint *getCenterGPS();

	void init_UL();
	void init_UR();
	void init_DL();
	void init_DR();
	double getArea();
	double getDensity();
	void print();
	void incrNBUsers();

	static void exploreChecksList(ListIndexCheckins *list);
	static void printCheck(Checkins *check);

	static double DistanceTo(double lat1, double lon1, double lat2, double lon2);


	//Getters
	Cell* getLd();
	Cell* getLu();
	Cell* getRd();
	Cell* getRu();
	int getCurrentLevel();
	Cell*getFather() ;
	int getNbUsers();
};
