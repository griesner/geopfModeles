/*
 * grid.h
 */
#include "cell.h"
#include <stdio.h>
#include <string.h>

class Grid{

private:
	int nbLvl;
	Cell *root;
	vector<Cell*> clusters;
	vector<Cell*> leafs;
	Checkins *tabChecks;
	int avgNBChecksInLeaf;

public:
	Grid(int nbLvl, int nbChecks);
	~Grid();
	void initAllChecks(string path);
	int putChecks(int indexCheck,Cell *root);
	void computeClusters(Cell *root);
	void computeClustersAlt(Cell *root);
	int exploreAllGrid(Cell *root);
	Cell* getRoot();
	vector<Cell*>getClusters();
	void printClusters();
	void gridOut(string path);
};
