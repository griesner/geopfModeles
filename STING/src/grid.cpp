/*
 * grid.cpp
 *
 *  Created on: 17 mai 2017
 *      Author: dosne
 */

#include "grid.h"

Grid::Grid(int nbLvl, int nbChecks)
{
	this->nbLvl = nbLvl;
	this->root = new Cell(true,NULL,'0',-180,180,-80,80,0,0);

	this->tabChecks = new Checkins[nbChecks];
	this->avgNBChecksInLeaf = 0;
}


Grid::~Grid()
{
	delete this->root;
	delete this->tabChecks;
	this->clusters.clear();
	this->leafs.clear();
}

int Grid::putChecks(int indexCheck,Cell *root)
{

	double longiFromCheck = this->tabChecks[indexCheck].coordinates->longitude;
	double latiFromCheck = this->tabChecks[indexCheck].coordinates->latitude;


	if(root->getCurrentLevel() == this->nbLvl)
	{
		root->addChecks(root->checks, indexCheck,latiFromCheck,longiFromCheck);
		root->incrNBUsers();
		this->leafs.push_back(root);
		return 1;
	}

	else
	{
		root->addChecks(root->checks, indexCheck,latiFromCheck,longiFromCheck);
		root->incrNBUsers();

		if(longiFromCheck< root->getCenterGPS()->longitude && latiFromCheck > root->getCenterGPS()->latitude)
		{
			//0
			if(root->getLu() == NULL)
				root->init_UL();
			return putChecks(indexCheck, root->getLu());
		}
		else if(longiFromCheck>root->getCenterGPS()->longitude && latiFromCheck > root->getCenterGPS()->latitude)
		{
			//1
			if(root->getRu() == NULL)
				root->init_UR();
			return putChecks(indexCheck, root->getRu());
		}
		else if(longiFromCheck<root->getCenterGPS()->longitude && latiFromCheck <root->getCenterGPS()->latitude)
		{
			//2
			if(root->getLd() == NULL)
				root->init_DL();
			return putChecks(indexCheck, root->getLd());
		}
		else if(longiFromCheck>root->getCenterGPS()->longitude && latiFromCheck <root->getCenterGPS()->latitude)
		{
			//3
			if(root->getRd() == NULL)
				root->init_DR();
			return putChecks(indexCheck, root->getRd());
		}

		else
		{
			return -1;
		}

	}


}

vector<Cell*>Grid::getClusters() {
	return clusters;
}

Cell* Grid::getRoot() {
	return this->root;
}

int Grid:: exploreAllGrid(Cell *root)
{

	if(root->getCurrentLevel() == this->nbLvl)
	{
		cout << "======Cell======" <<endl;
		root->print();
		cout << "======Bucket======" <<endl;
		//		Cell::exploreChecksList(root->checks);
		return 1;
	}

	else
	{
		cout << "======Cell======" <<endl;
		root->print();
		cout << "======Bucket======" <<endl;
		//		Cell::exploreChecksList(root->checks);
		if(root->getRu() != NULL)
			return exploreAllGrid(root->getRu());
		else if(root->getLu() != NULL)
			return exploreAllGrid(root->getLu());
		else if(root->getRd() != NULL)
			return exploreAllGrid(root->getRd());
		else if(root->getLd() != NULL)
			return exploreAllGrid(root->getLd());
	}
}

void Grid::computeClusters(Cell* root) {


	if(root->getCurrentLevel() == this->nbLvl ||
			(root->getLd() == NULL && root->getRd() == NULL && root->getLu() == NULL && root->getRu() == NULL))
	{
		this->clusters.push_back(root);

	}

	else if(root->getFather() != NULL && root->getCurrentLevel() > this->nbLvl)
	{

		if(root->getFather()->getDensity() < root->getDensity() &&  root->getNbUsers() <= this->avgNBChecksInLeaf)
		{
			this->clusters.push_back(root);
		}

		else
		{
			if(root->getLd() != NULL)
				computeClusters(root->getLd());
			if(root->getRd() != NULL)
				computeClusters(root->getRd());
			if(root->getLu() != NULL)
				computeClusters(root->getLu());
			if(root->getRu() != NULL)
				computeClusters(root->getRu());
		}

	}

	else
	{
		if(root->getLd() != NULL)
			computeClusters(root->getLd());
		if(root->getRd() != NULL)
			computeClusters(root->getRd());
		if(root->getLu() != NULL)
			computeClusters(root->getLu());
		if(root->getRu() != NULL)
			computeClusters(root->getRu());
	}
}



void Grid::computeClustersAlt(Cell* root) {

	//If we are in a leaf
	if(root->getCurrentLevel() == this->nbLvl ||
			(root->getLd() == NULL && root->getRd() == NULL && root->getLu() == NULL && root->getRu() == NULL))
	{
		this->clusters.push_back(root);

	}

	//Otherwise inter node
	else
	{
		bool isCluster = false;

		if(root->getLd() != NULL)
			if(root->getLd()->getDensity() < root->getDensity())
				isCluster = true;
		if(root->getRd() != NULL)
			if(root->getRd()->getDensity() < root->getDensity())
				isCluster = true;
		if(root->getLu() != NULL)
			if(root->getLu()->getDensity() < root->getDensity())
				isCluster = true;
		if(root->getRu() != NULL)
			if(root->getRu()->getDensity() < root->getDensity())
				isCluster = true;

		//If for at least one son density(Node) > density(son)
		if(isCluster)
			this->clusters.push_back(root);

		//Otherwise go to lower level
		else{
			if(root->getLd() != NULL)
				if(root->getLd()->getDensity() >= root->getDensity())
					computeClusters(root->getLd());

			if(root->getRd() != NULL)
				if(root->getRd()->getDensity() >= root->getDensity())
					computeClusters(root->getRd());

			if(root->getLu() != NULL)
				if(root->getLu()->getDensity() >= root->getDensity())
					computeClusters(root->getLu());

			if(root->getRu() != NULL)
				if(root->getRu()->getDensity() >= root->getDensity())
					computeClusters(root->getRu());

		}

	}


}



void Grid::initAllChecks(string path) {

	FILE *fileptr = fopen(path.c_str(),"r");
	int user, poi,time;
	char *gps = new char[50];
	char *gps_coord[2];
	int compt = 0;
	while ((fscanf(fileptr, "%d\t%s\t%d", &user, gps, &time) != EOF)) {
		gps_coord[0] = strtok(gps, ",");
		gps_coord[1] = strtok(NULL, ",");

		Checkins check;
		check.userID = user;
		check.poiID  = -1;
		//		check.poiID  = poi;
		check.time = time;
		GpsPoint *point = new GpsPoint;
		//		point->latitude = atof( gps_coord[1]);
		//		point->longitude = atof( gps_coord[0]);
		point->latitude =   strtod(gps_coord[0],NULL);
		point->longitude =   strtod(gps_coord[1],NULL);
		check.coordinates = point;
		this->tabChecks[compt] = check;
		//		cout << check.userID <<" " << check.poiID  <<" "<<point->latitude <<","<<point->longitude<<" "<<check.time  <<endl;
		this->putChecks(compt,this->root);
		compt++;
	}

	for(int i = 0;i<this->leafs.size();i++)
	{
		this->avgNBChecksInLeaf += leafs[i]->getNbUsers();
	}

	this->avgNBChecksInLeaf /= leafs.size();
	cout << this->avgNBChecksInLeaf << endl;

}

void Grid::printClusters() {

	int compt = 0;
	for(int i = 0;i<this->clusters.size();i++)
	{
		cout << "id : " << this->clusters[i]->id << endl;
		compt += this->clusters[i]->getNbUsers() ;
	}
	cout << "id : " << compt << endl;

}

void Grid::gridOut(string path) {


	stringstream file_path;
	file_path << path;
	file_path<< "/out";
	IndexChecks *tmp = NULL;
	ofstream myfile (file_path.str());

	if (myfile.is_open())
	{
		for(int i = 0;i<this->clusters.size();i++)
		{
			tmp = this->clusters[i]->checks->firstElem;
			while(tmp)
			{
				myfile << this->tabChecks[tmp->index].userID <<"\t" << this->clusters[i]->id << "\t"
						<< this->clusters[i]->getAVGCenterGPS()->latitude << ","
						<< this->clusters[i]->getAVGCenterGPS()->longitude << "\t"
						<< this->tabChecks[tmp->index].time;
				myfile << "\n";
				tmp = tmp->next;
			}

		}
	}

	delete tmp;
	myfile.close();

}
